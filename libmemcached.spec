Name:          libmemcached
Summary:       Client library and command line tools for memcached server
Version:       1.0.18
Release:       20
License:       BSD
URL:           http://libmemcached.org/
#origin source:https://launchpad.net/libmemcached/1.0/1.0.18/+download/libmemcached-1.0.18.tar.gz
#remove hsieh.cc which license of it is not free
Source0:       libmemcached-1.0.18.tar.gz

BuildRequires: gcc-c++ cyrus-sasl-devel flex bison python-sphinx memcached
BuildRequires: systemtap-sdt-devel libevent-devel

Provides:      bundled(bobjenkins-hash)
Provides:      libmemcached-libs
Obsoletes:     libmemcached-libs < %{version}-%{release}

Patch0001:     libmemcached-fix-linking-with-libpthread.patch
Patch0002:     libmemcached-build.patch
Patch0003:     libmemcached-fix-memdump-version.patch
Patch0004:     libmemcached-Ignore-specific-errors-from-memcached_dump.patch
Patch0005:     libmemcached-fix-multiple-definition.patch
Patch0006:     libmemcached-memcapable-fix-version-test-with-v1.6+.patch
Patch0007:     libmemcached-memcapable-fix-quit-test-with-v1.6+.patch
Patch0008:     libmemcached-tests-fix-clients-memcapable.patch


%description
Memcached is an in-memory key-value store for small arbitrary data
(strings, objects) from results of database calls, API calls, or
page rendering. libmemcached is a C/C++ client library and tools
for the memcached server (http://memcached.org/).

%package        devel
Summary:        Header files and development libraries for libmemcached
Requires:       libmemcached = %{version}-%{release}
Requires:       pkgconfig cyrus-sasl-devel

%description    devel
This package contains the header files and development libraries
for libmemcached. If you like to develop programs using libmemcached,
you will need to install libmemcached-devel.

%package        help
Summary:   Help document for the libmemcached package
Buildarch: noarch

%description    help
Help document for the libmemcached package.

%prep
%autosetup -n libmemcached-%{version} -p1

mkdir examples
cp -p tests/*.{cc,h} examples/

%build
%configure \
   --with-memcached=%{_bindir}/memcached \
   --enable-sasl --enable-libmemcachedprotocol \
   --enable-memaslap --enable-dtrace --disable-static

%make_build V=1


%install
%make_install

if [ ! -d %{buildroot}%{_mandir}/man1 ]; then
   install -D -p -m 644 man/*1 %{buildroot}%{_mandir}/man1
   install -D -p -m 644 man/*3 %{buildroot}%{_mandir}/man3
fi


%check
%if %{?_with_tests:1}%{!?_with_check:0}
make test
%endif

%files
%{_bindir}/mem*
%exclude %{_libdir}/lib*.la
%doc COPYING COPYING AUTHORS README THANKS TODO ChangeLog
%{_libdir}/libhashkit.so.2*
%{_libdir}/libmemcached.so.11*
%{_libdir}/libmemcachedprotocol.so.0*
%{_libdir}/libmemcachedutil.so.2*

%files devel
%doc examples
%{_includedir}/libmemcached
%{_includedir}/libmemcached*.0
%{_includedir}/libhashkit*/
%{_libdir}/libhashkit.so
%{_libdir}/libmemcached*.so
%{_libdir}/pkgconfig/libmemcached.pc
%{_datadir}/aclocal/ax_libmemcached.m4

%files help
%{_mandir}/man1/mem*
%{_mandir}/man3/libmemcached*
%{_mandir}/man3/libhashkit*
%{_mandir}/man3/memcached*
%{_mandir}/man3/hashkit*

%changelog
* Tue Aug 24 2021 lingsheng <lingsheng@huawei.com> - 1.0.18-20
- Fix memcapable tests

* Wed Aug 04 2021 Liu Yinsi <liuyinsi@163.com> - 1.0.18-19
- fix multiple definition to support gcc 10

* Mon Aug 02 2021 Liu Yinsi <liuyinsi@163.com> - 1.0.18-18
- compile with -fcommon to support gcc 10

* Fri Oct 30 2020 lingsheng <lingsheng@huawei.com> - 1.0.18-17
- Fix memdump unexpected errors

* Fri Feb 28 2020 zhouyihang <zhouyihang1@huawei.com> - 1.0.18-16
- Package init
